<?php
	if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$human = intval($_POST['human']);
		$from = 'My site'; 
		$to = 'vly.2814@gmail.com'; 
		$subject = 'Message from Contact Demo ';
		
		$body ="From: $name\n E-Mail: $email\n Message:\n $message";

		// Check if name has been entered
		if (!$_POST['name']) {
			$errName = 'Va rugam introduceti numele';
		}
		
		// Check if email has been entered and is valid
		if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Va rugam introfuceti o adresa valida de e-mail';
		}
		
		//Check if message has been entered
		if (!$_POST['message']) {
			$errMessage = 'Va rugam introduceti mesajul';
		}
		//Check if simple anti-bot test is correct
		if ($human !== 5) {
			$errHuman = 'Anti-spam este incorect';
		}

// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
	if (mail ($to, $subject, $body, $from)) {
		$result='<div class="alert alert-success">Multumim! Mesajul va fi verificat de un administrator, apoi va fi postat.</div>';
	} else {
		$result='<div class="alert alert-danger">A aparut o eroare la trimiterea mesajului. Va rugam incercati mai tarziu.</div>';
	}
}
	}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Bootstrap contact form with PHP example by BootstrapBay.com.">
    <meta name="author" content="BootstrapBay.com">
    <title>Adauga o intrebare</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="java/jquery.min.js"></script>
    <script src="java/skel.min.js"></script>
    <script src="java/skel-layers.min.js"></script>
    <script src="java/init.js"></script>
    
    <noscript>
			<link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/mybutton.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
  
  <link href="https://cdn.mdn.mozilla.net/static/build/styles/samples.37902ba3b7fe.css" rel="stylesheet" type="text/css">
        <link href="css/notificare.css" rel="stylesheet" type="text/css">
         <script src="java/notificare.js"></script>
</head>
  <body>
          <header id="header" class="skel-layers-fixed">
        <h1 style="font-size:18px"  ><a href="main.php">Acasa</a></h1>
        <nav id="nav">
            <ul>
                   <li><a href="../main.html" class="button special">ACASA</a></li>
                <li> <a href="../TIPURI%20DE%20ALERGII.html" >TIPURI DE ALERGII</a> </li>
                <li> <a href="../ASISTENT.html" >ASISTENT</a> </li>
                <li><a href="../harta%20judete.html" >LOCATIE</a></li>
                <li><a href="../FORUM.php">INTREBARI</a></li>
                <li><a href="../login.php" style="color: red;">Deconectare</a></li>
            </ul>
        </nav>
         
    </header>
<style>
      .button-intrebare {
    cursor: pointer;background-color: #008CBA;
    border: none;
    color: white;
    border-radius: 6px;
    text-align: center;
    padding: 7px;
    display: inline-block;
    font-size: 16px;
       
          margin-left: 160px;
          margin-top: -52px;
    
    }
        </style>    
</head>
  <body>
  	<div class="container">
  		<div class="row">
  			<div class="col-md-6 col-md-offset-3">
  				<h1 class="page-header text-center">Adauga un comentariu</h1>
				<form class="form-horizontal" role="form" method="post" action="lul.php">
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Nume</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="name" name="name" placeholder="Nume si prenume" value="<?php echo htmlspecialchars($_POST['name']); ?>">
							<?php echo "<p class='text-danger'>$errName</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo htmlspecialchars($_POST['email']); ?>">
							<?php echo "<p class='text-danger'>$errEmail</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="message" class="col-sm-2 control-label">Mesaj</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
							<?php echo "<p class='text-danger'>$errMessage</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="human" name="human" placeholder="Raspuns">
							<?php echo "<p class='text-danger'>$errHuman</p>";?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							
                            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
                            
						</div>

					</div>
					
					<div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
							<?php echo $result; ?>	
						</div>   
					</div>
                   
				</form> 
               
			</div>
		</div>
	</div>   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>
